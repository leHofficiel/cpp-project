#include <iostream>

const int score[4] = {0, 15, 30, 40};

// nbWinPlayer = nombre d'échange gagné par un joueur sur un jeu
void get_score(int nbWinPlayer1, int nbWinPlayer2){
    if(nbWinPlayer1 < 4 && nbWinPlayer2 < 4){
        printf("Player 1 : %d | %d : Player 2\n", 
            score[nbWinPlayer1],
            score[nbWinPlayer2]
        );
    }
    else if(nbWinPlayer1 == nbWinPlayer2){
        printf("Egalité \n");
    }
    else if(nbWinPlayer1 > nbWinPlayer2){
        if( nbWinPlayer1 - nbWinPlayer2 >= 2){ printf("Victoire du joueur 1\n");}
        else{ printf("Avantage pour le joueur 1\n"); }
    }
    else{
        if( nbWinPlayer2 - nbWinPlayer1 >= 2){ printf("Victoire du joueur 2\n");}
        else{ printf("Avantage pour le joueur 2\n"); }
    }
}

int main(){

    int nbWinPlayer1;
    int nbWinPlayer2;

    printf("Nombre d'échange gagné par le joueur 1 : ");    
    std::cin >> nbWinPlayer1;
    printf("Nombre d'échange gagné par le joueur 2 : ");
    std::cin >> nbWinPlayer2;    

    get_score(nbWinPlayer1, nbWinPlayer2);
   
    return EXIT_SUCCESS;
}