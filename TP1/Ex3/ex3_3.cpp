#include <iostream>

using namespace std;

int main(){

    int borneInf;
    int borneSup;
    printf("Valeur min : ");
    cin >> borneInf;
    printf("Valeur max : ");
    cin >> borneSup;

    int value = (borneSup+borneInf)/2;    
    char result;
    bool found = false;
    do{
        printf("%d ? ( + / - / = ) : ", value);
        cin >> result;
        switch(result){
            case '=':
                found = true; break;
            case '+':            
                borneInf = value;
                value = (borneSup+value) / 2;
                break;
            case '-':
                borneSup = value;
                value = (value+borneInf) / 2;
                break;
        }        
    }
    while(!found);



    return EXIT_SUCCESS;
}