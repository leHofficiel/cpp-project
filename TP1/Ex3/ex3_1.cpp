#include <iostream>
#include <string>

using namespace std;

void upperString(string& str){
    for(int i=0; i<(int)str.size(); i++){                
        str[i] = toupper((int)str[i]);
    }
}

int main(){

    string name;    
    string firstName;

    printf("Votre nom et prénom : ");
    cin >> name >> firstName;    

    upperString(name);
    firstName[0] = toupper((int)firstName[0]);    
    
    cout << "Bonjour " << name << " " << firstName <<  endl;        

}