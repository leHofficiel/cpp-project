#include "forme.hpp"
#include <algorithm>
#include <numeric>

const float FLOAT_PRECISSION = 0.001f;

Triangle::Triangle(){}

Triangle::Triangle(Point point1, Point point2, Point point3){
    this->point1 = point1;
    this->point2 = point2;
    this->point3 = point3;
}

Point Triangle::GetPoint1() const { return this->point1; }
void Triangle::SetPoint1(Point point){ this->point1 = point ; }

Point Triangle::GetPoint2() const { return this->point2; }
void Triangle::SetPoint2(Point point){ this->point2 = point; }

Point Triangle::GetPoint3() const { return this->point3; }
void Triangle::SetPoint3(Point point){ this->point3 = point; }


// Base du triangle : coté le plus grand
float Triangle::Base() const {
    std::vector<float> list = this->Longueurs();     
    return *std::max_element(list.begin(), list.end());      
}
// Hauteur du triangle issue de la base
float Triangle::Hauteur() const {
    return this->Surface()*2/this->Base();
}
float Triangle::Surface() const {    
    std::vector<float> longueurs = this->Longueurs();
    float demiPerimetre = std::accumulate(longueurs.begin(), longueurs.end(), 0.0f) * 0.5;       
    return sqrt( demiPerimetre * 
        (demiPerimetre - longueurs[0]) *
        (demiPerimetre - longueurs[1]) *
        (demiPerimetre - longueurs[2])        
    );
}
std::vector<float> Triangle::Longueurs() const {
    return std::vector<float> {
        Point::DistanceEntre(this->point1, this->point2),
        Point::DistanceEntre(this->point1, this->point3),
        Point::DistanceEntre(this->point2, this->point3)
    };
}


bool Triangle::EstIsocele() const {
    std::vector<float> longueurs = this->Longueurs();
    return longueurs[0]==longueurs[1] || longueurs[0]==longueurs[2] || longueurs[1]==longueurs[2];
}
bool Triangle::EstRectangle() const {    
    std::vector<float> list = this->Longueurs();
    float hypothenus = this->Base();
   
    for(int i=list.size()-1; i>=0; i--){        
        if(list[i] == hypothenus){ list.erase(list.begin()+i); }
    }
    
    if(list.size() != 2){ return false; }    
   
    float result = fabs(hypothenus*hypothenus - (list[0]*list[0]+list[1]*list[1]));
    return result >= 0 && result <= FLOAT_PRECISSION;  
}

bool Triangle::EstEquilateral() const {
    std::vector<float> list = this->Longueurs();
    for(std::size_t i=1; i<list.size(); i++){
        float substraction = fabs(list[i] - list[i-1]);
        if( substraction < 0 || substraction > FLOAT_PRECISSION ){ return false; }
    }
    return true;
}

void Triangle::Afficher() const {
    std::string strLongueurs = "[";
    std::vector<float> longueurs = this->Longueurs();
    strLongueurs += "1->2 : "+ std::to_string(longueurs[0])+ ", ";
    strLongueurs += "1->3 : "+ std::to_string(longueurs[1])+ ", ";
    strLongueurs += "2->3 : "+ std::to_string(longueurs[2])+ " ]";


    std::cout << "Triangle : " << std::endl
        << "    Point 1 : "      << this->point1.Afficher() << std::endl
        << "    Point 2 : "      << this->point2.Afficher() << std::endl
        << "    Point 3 : "      << this->point3.Afficher() << std::endl
        << "    Base : "         << this->Base()            << std::endl
        << "    Hauteur : "      << this->Hauteur()         << std::endl
        << "    Surface : "      << this->Surface()         << std::endl
        << "    Longueurs : "    << strLongueurs            << std::endl
        << "    Isocèle : "      << this->EstIsocele()      << std::endl
        << "    Rectangle : "    << this->EstRectangle()    << std::endl
        << "    Equilatérale : " << this->EstEquilateral()  << std::endl;
}


