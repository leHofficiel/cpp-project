#include "forme.hpp"
#include "math.h"


Cercle::Cercle(int diametre, Point point){
    this->diametre = diametre;
    this->centre = point;
    this->rayon = diametre/2;
}

int Cercle::GetDiametre() const { return this->diametre; }
void Cercle::SetDiametre(int diametre){ this->diametre = diametre; }

Point Cercle::GetCentre() const { return this->centre; }
void Cercle::SetCentre(Point point){ this->centre = point; }


float Cercle::Perimetre() const { 
    return 2*M_PI*this->rayon;
}
float Cercle::Surface() const {
    return M_PI*(this->rayon*this->rayon);
}

bool Cercle::EstDessus(const Point& point) const {
    return Point::DistanceEntre(this->centre, point) == this->rayon;
}
bool Cercle::EstInclue(const Point& point) const {    
    return Point::DistanceEntre(this->centre, point) < this->rayon;
}

void Cercle::Afficher() const {
    std::cout << "Cercle : " << std::endl
        << "    Diamètre : "  << this->diametre          << std::endl        
        << "    Centre : "    << this->centre.Afficher() << std::endl
        << "    Périmètre : " << this->Perimetre()       << std::endl
        << "    Surface : "   << this->Surface()         << std::endl;
}
