cmake_minimum_required(VERSION 3.5)

project(QtApplication VERSION 0.1 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Widgets REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Widgets REQUIRED)

set(PROJECT_SOURCES
        main.cpp        
        # -- CONTROLLERS --
        Controllers/GameMode.h
        Controllers/GameController.h
        Controllers/GameController.cpp

        Controllers/GameModes/Ladies.h
        Controllers/GameModes/Ladies.cpp
        Controllers/GameModes/Morpion.h
        Controllers/GameModes/Morpion.cpp
        Controllers/GameModes/Othello.h
        Controllers/GameModes/Othello.cpp        
        Controllers/GameModes/Puissance4.h        
        Controllers/GameModes/Puissance4.cpp        

        # -- MODELS --
        Models/Grid.h
        Models/Grid.cpp
        Models/Player.h
        Models/Structs.h

        # -- UTILS -- 
        Utils/Sauvegarder.h
        Utils/Sauvegarder.cpp

        # -- VIEWS --
        Views/UserInterface.h
        Views/CLI/CLI.h
        Views/CLI/CLI.cpp
        Views/GUI/mainwindow.cpp
        Views/GUI/mainwindow.h
        Views/GUI/mainwindow.ui
)

if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(QtApplication
        MANUAL_FINALIZATION
        ${PROJECT_SOURCES}
    )
# Define target properties for Android with Qt 6 as:
#    set_property(TARGET QtApplication APPEND PROPERTY QT_ANDROID_PACKAGE_SOURCE_DIR
#                 ${CMAKE_CURRENT_SOURCE_DIR}/android)
# For more information, see https://doc.qt.io/qt-6/qt-add-executable.html#target-creation
else()
    if(ANDROID)
        add_library(QtApplication SHARED
            ${PROJECT_SOURCES}
        )
# Define properties for Android with Qt 5 after find_package() calls as:
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
    else()
        add_executable(QtApplication
            ${PROJECT_SOURCES}
        )
    endif()
endif()

target_link_libraries(QtApplication PRIVATE Qt${QT_VERSION_MAJOR}::Widgets)

set_target_properties(QtApplication PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

if(QT_VERSION_MAJOR EQUAL 6)
    qt_finalize_executable(QtApplication)
endif()
