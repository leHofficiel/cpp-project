#include "GameController.h"
#include <memory>
#include <utility>

std::shared_ptr<GameController> GameController::_instance = nullptr;

//
// CONSTRUCTOR
//

std::shared_ptr<GameController> GameController::GetInstance() {
    if(_instance==nullptr){
        //_instance = new GameController();
        _instance = std::shared_ptr<GameController>(new GameController());
    }
    return _instance;
}

//
// PUBLIC
//

Player GameController::GetOtherPlayer(const Player& player) const {
    for(Player other : players){
        if( other.GetSymbol() != player.GetSymbol() ){
            return other;
        }
    }
    return {};
}

void GameController::Exec(){
    connect(this, SIGNAL(CreateNewGame_Signal()), this, SLOT(CreateNewGame_Action()));
    connect(this, SIGNAL(LoadGame_Signal(std::string)), this, SLOT(LoadGame_Action(std::string)));
    connect(this, SIGNAL(GameChosed_Signal(int)), this, SLOT(GameChosed_Action(int)));
    connect(this, SIGNAL(CreatePlayer_Signal(std::string, bool, std::string, bool)), this, SLOT(CreatePlayer_Action(std::string, bool, std::string, bool)));
    connect(this, SIGNAL(GoToEndPage_Signal()), this, SLOT(GoToEndPage_Action()));
    connect(this, SIGNAL(Replay_Signal()), this, SLOT(Replay_Action()));
    connect(this, SIGNAL(ReplayWithNewPlayers_Signal()), this, SLOT(ReplayWithNewPlayers_Action()));
    connect(this, SIGNAL(Exit_Signal()), this, SLOT(Exit_Action()));
    connect(this, SIGNAL(Save_Signal(std::string)), this, SLOT(Save_Action(std::string)));

    userInterface->DisplayStartMenu();

    this->exec();    
}

Position GameController::RequestInputByXY(){
    userInterface->DisplayRequestInputByXY(grid.GetLineNumber(), grid.GetColumnNumber());
    WaitForInputSignal();
    return inputPosition;
}

Position GameController::RequestInputByColumn(){
    userInterface->DisplayRequestInputByCols(grid.GetColumnNumber());
    WaitForInputSignal();
    inputPosition.line = 0;
    return inputPosition;
}

Position GameController::RequestInputByLine(){
    userInterface->DisplayRequestInputByLine(grid.GetLineNumber());
    WaitForInputSignal();
    inputPosition.column = 0;
    return inputPosition;
}

//
// SLOTS
//

void GameController::CreateNewGame_Action(){
    userInterface->DisplayGameMenu();
}

void GameController::GameChosed_Action(int gameId){
    InitGameMode(gameId);
    userInterface->DisplayPlayerMenu(gameType->IsBotEnable());
}

void GameController::CreatePlayer_Action(std::string nameP1, bool isBotP1, std::string nameP2, bool isBotP2){
    this->players.clear();
    CreatePlayer(nameP1, syms[0], isBotP1);
    CreatePlayer(nameP2, syms[1], isBotP2); 

    InitGrid();

    this->currentPlayerIndex=0;
    
    userInterface->DisplayGridView(grid.GetLineNumber(), grid.GetColumnNumber());
    userInterface->DisplayMainMessage(gameType->GetGameName());

    PlayGame();  
}

void GameController::LoadGame_Action(std::string file){
    
    QJsonObject obj = Sauvegarde().Charger(QString::fromStdString(file)); 

    if(obj.isEmpty()){
        userInterface->DisplayWarningMessage("Le fichier n'est pas valide");
        return;
    }

    if(!obj.contains("GameType") || !obj.contains("Players") || !obj.contains("Grid") || !obj.contains("CurrentPlayerIndex")){
        userInterface->DisplayWarningMessage("Le fichier n'est pas une sauvegarde");
        return;
    }
        
    InitGameMode(obj.value("GameType").toInt());
    LoadPlayers(obj.value("Players").toArray());
    LoadGrid(obj.value("Grid").toArray());

    this->currentPlayerIndex = obj.value("CurrentPlayerIndex").toInt();

    userInterface->DisplayGridView(grid.GetLineNumber(), grid.GetColumnNumber());
    userInterface->DisplayMainMessage(gameType->GetGameName());

    PlayGame();   
}

void GameController::GoToEndPage_Action(){
    userInterface->DisplayEndMenu();
}

void GameController::Replay_Action(){
    InitGrid();
    userInterface->DisplayGridView(grid.GetLineNumber(), grid.GetColumnNumber());
    userInterface->DisplayMainMessage(gameType->GetGameName());
    this->currentPlayerIndex = 0;
    PlayGame();
}

void GameController::ReplayWithNewPlayers_Action(){
    userInterface->DisplayPlayerMenu(gameType->IsBotEnable());
}

void GameController::Exit_Action(){
    emit this->quit();
}

void GameController::Save_Action(std::string path){
    Sauvegarde().Sauvegarder(ToJson(), QString::fromStdString(path));
}

void GameController::PlayerInput_Action(Position pos){
    inputPosition = pos;
    emit Quit_loop();
}

//  
// PRIVATE
//

void GameController::WaitForInputSignal(){
    QEventLoop loop;
    connect( this, SIGNAL(PlayerInput_Signal(Position)), this, SLOT(PlayerInput_Action(Position)) );
    connect( this, SIGNAL(Quit_loop()), &loop, SLOT(quit()) );
    loop.exec();
}

void GameController::LoadPlayers(QJsonArray players)
{    
    for (int i = 0; i < players.size(); i++)
    {
        QJsonObject player = players.at(i).toObject();
        std::string name = player.value("name").toString().toStdString();
        std::string symbol = player.value("symbol").toString().toStdString();
        bool isBot = player.value("isBot").toBool();
        CreatePlayer(name, symbol[0], isBot);
    }
}

void GameController::LoadGrid(QJsonArray qGrid)
{
    this->grid = Grid(this->gameType->GetGridLineNumber(), this->gameType->GetGridColumnNumber());    
    for(int i=0; i< grid.GetColumnNumber()*grid.GetLineNumber(); i++ ){
        QJsonObject cell = qGrid.at(i).toObject();
        std::string symbol = cell.value("symbol").toString().toStdString();
        this->grid.Set(i, symbol[0]);
    }  
}

void GameController::InitGameMode(int gameId){
    this->gameId = gameId;
    switch (gameId)
    {
        case 0:
            gameType = std::make_shared<Morpion>();
            break;
        case 1:
            gameType = std::make_shared<Puissance4>();
            break;
        case 2:
            gameType = std::make_shared<Othello>();
            break;
        default:
            gameType = std::make_shared<Ladies>();
            break;
    }
}

void GameController::CreatePlayer(std::string name, char symbol, bool isBot){
    players.push_back(Player(name, symbol, isBot));        
}

void GameController::InitGrid(){
    this->grid = Grid(this->gameType->GetGridLineNumber(), this->gameType->GetGridColumnNumber());
    gameType->GridInitState();
}

void GameController::PlayGame(){
    bool run = true;
    while (run)
    {
        //for(const auto& player: this->players){
            PlayRound();

            if(grid.GetEmptyCells().empty()){
                FindWinner();
                run = false;
            }
            else run = FindGameState(players[currentPlayerIndex]);

            if(!run) break;

            if(nextPlayer){
                this->currentPlayerIndex = (this->currentPlayerIndex+1)%players.size();
            }
            nextPlayer = true;

        //}
    }
    userInterface->DisplayGrid(this->grid);
}

void GameController::PlayRound(){   

    userInterface->DisplayGrid(this->grid);
    
    Player player = players[currentPlayerIndex];
    userInterface->DisplayMessage(player.GetName() + " (" + player.GetSymbol() + ") " + "à toi de jouer !");    
    
    if(player.IsBot()){
        gameType->AIPlay();
    }
    else{
        gameType->Play();
    }

}

void GameController::FindWinner(){
    for(int i=0; i<players.size(); i++){
        this->currentPlayerIndex = i;
        if(gameType->GridState() == GameState::WIN){
            userInterface->DisplayFinalState(players[i].GetName()+" a gagné !!");            
            return;
        }
    }    
    userInterface->DisplayFinalState("Egalité !!");
}

bool GameController::FindGameState(const Player& player){
    bool result = true;
    switch (gameType->GridState()){
        case GameState::WIN:            
            userInterface->DisplayFinalState(player.GetName()+" à gagné");
            result = false;
            break;
        case GameState::EQUALITY:            
            userInterface->DisplayFinalState("Egalité");
            result = false;
            break;
        case GameState::NOTHING:
            break;
        case GameState::DEFEAT:
            auto p = GetOtherPlayer(player);            
            userInterface->DisplayFinalState(p.GetName()+" à gagné");
            break;
    }
    return result;
}

QJsonObject GameController::ToJson()
{
    QJsonObject json;    
    json.insert("GameType", gameId);
    json.insert("CurrentPlayerIndex", currentPlayerIndex);

    QJsonObject playersJson;
    QJsonArray playersArray;
    for (const auto& player : players)
    {
        QJsonObject playerJson;
        playerJson["name"] = QJsonValue( QString::fromStdString(player.GetName()) ) ;
        playerJson["symbol"] = QJsonValue( QString(player.GetSymbol()) ) ;
        playerJson["isBot"] =  QJsonValue( player.IsBot() ) ;
        playersArray.append(playerJson);
    }
    json.insert("Players", playersArray);

    QJsonObject gridJson;
    QJsonArray gridArray;
    for(int i=0; i<grid.GetLineNumber()*grid.GetColumnNumber(); i++){
        QJsonObject cell;
        cell["symbol"] = QJsonValue(QString(grid.GetCell(i)));
        gridArray.append(cell);
    }  
    json.insert("Grid", gridArray);    

    return json;    
}




