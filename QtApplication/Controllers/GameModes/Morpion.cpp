#include "Morpion.h"
#include "../GameController.h"

Morpion::Morpion(){}

void Morpion::Play() {
    std::shared_ptr<GameController> ctrl = GameController::GetInstance();
    std::shared_ptr<UserInterface> userInterface = ctrl->GetUserInterface();
    Grid& grid = ctrl->GetGrid();

    Position pos;
    bool isCorrectInput = false;
    while(!isCorrectInput){
        pos = ctrl->RequestInputByXY();
        if(grid.IsEmpty(pos)){
            isCorrectInput = true;
        }
        else{
            userInterface->DisplayWarningMessage("Position incorrect");
        }
    }
    
    grid.Place(pos, ctrl->GetCurrentPlayer().GetSymbol());
}

void Morpion::AIPlay() const {
    std::shared_ptr<GameController> ctrl = GameController::GetInstance();
    Grid& grid = ctrl->GetGrid();

    std::random_device r;
    std::vector<Position> emptyCells =grid.GetEmptyCells();
    auto gen = std::bind(std::uniform_int_distribution<>(0,emptyCells.size()-1),std::default_random_engine(r()));    
        
    Position pos;    
    do{
        pos = emptyCells[gen()];
    }while(!grid.IsEmpty(pos));
    
    grid.Place(pos, ctrl->GetCurrentPlayer().GetSymbol());    
}

GameState Morpion::GridState() const {
    std::shared_ptr<GameController> ctrl = GameController::GetInstance();
    if(Win(ctrl->GetGrid(), ctrl->GetCurrentPlayer().GetSymbol())){
        return GameState::WIN;
    }
    else if(Equality(ctrl->GetGrid())){
        return GameState::EQUALITY;
    }
    return GameState::NOTHING;    
}

void Morpion::GridInitState() const {}


//
// PRIVATE
//

bool Morpion::Win(const Grid& grid, char symbol) const {
    int win[8][3] = {{0, 1, 2}, // Check first row.
                     {3, 4, 5}, // Check second Row
                     {6, 7, 8}, // Check third Row
                     {0, 3, 6}, // Check first column
                     {1, 4, 7}, // Check second Column
                     {2, 5, 8}, // Check third Column
                     {0, 4, 8}, // Check first Diagonal
                     {2, 4, 6}}; // Check second Diagonal
    // Check all possible winning combinations
    for (auto & i : win){
        auto a = grid.GetCell(i[0]);
        auto b = grid.GetCell(i[1]);
        auto c = grid.GetCell(i[2]);
        if (a == b && b == c && a == symbol){
            return true;
        }
    }
    return false;
}

bool Morpion::Equality(const Grid& grid) const {
    return grid.GetEmptyCells().empty();
}
