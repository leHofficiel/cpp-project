#ifndef GAME_OTHELLO_H
#define GAME_OTHELLO_H

#include "../GameMode.h"

class Othello: public GameMode {
    public:
        Othello();
        int GetGridLineNumber() const override { return GRID_LINES; }
        int GetGridColumnNumber() const override { return GRID_COLUMNS; }
        int GetPlayerNumber() const override { return PLAYER_NUMBER; }
        bool IsBotEnable() const override { return false; }
        std::string GetGameName() const override { return "Othello"; }

        void GridInitState() const override;
        void Play() override;
        void AIPlay() const override;
        GameState GridState() const override;


    private:
        const int GRID_LINES = 8;
        const int GRID_COLUMNS = 8;
        const int PLAYER_NUMBER = 2;


        GameState GetGameState(const Grid& grid, char symbol, char enemy) const;

        bool EstSandwichVertical(Grid& grid, Position &position, char player, char enemy) const;
        bool EstSandwichHorizontal(Grid& grid, Position &posGetOtherPlayerition, char player, char enemy) const;
        bool EstSandwichDiagonal(Grid& grid, Position &position, char player, char enemy) const;

        void RetournementVertical(Grid& grid, int column, int line, int line2, char player) const;
        void RetournementHorizontal(Grid& grid, int line, int column, int column2, char player) const;
        void RetournementDiagonal(Grid& grid, int line, int column, int line2, int column2, char player, int diagonal) const;


};

#endif //GAME_OTHELLO_H
