#include "Controllers/GameController.h"
#include "Views/GUI/mainwindow.h"
#include "Views/CLI/CLI.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // std::shared_ptr<CLI> userInterface = std::make_shared<CLI>();
    std::shared_ptr<MainWindow> userInterface = std::make_shared<MainWindow>();
    
    GameController::GetInstance()->SetUserInterface( userInterface );
    userInterface->show();
    GameController::GetInstance()->Exec();

    return EXIT_SUCCESS;
}
