#include "Sauvegarder.h"

#include <iostream>


Sauvegarde::Sauvegarde()
{

}

QJsonObject Sauvegarde::Charger(const QString &fichier)
{
    QFile jsonFile(fichier);
    jsonFile.open(QFile::ReadOnly);
    QJsonDocument doc = QJsonDocument().fromJson(jsonFile.readAll());
    return doc.object();
}

void Sauvegarde::Sauvegarder(QJsonObject obj, const QString& fichier)
{
    QJsonDocument doc(obj);

    QFile jsonFile(fichier);
    jsonFile.open(QFile::WriteOnly);
    jsonFile.write(doc.toJson());
}

// QJsonObject player = players.at(i).toObject();
//         std::string name = player.value("name").toString().toStdString();
//         std::string symbol = player.value("symbol").toString().toStdString();
//         bool isBot = player.value("isBot").toBool();
//         this->players.push_back(Player(name, symbol));