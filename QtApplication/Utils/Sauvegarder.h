#ifndef SAUVEGARDE_H
#define SAUVEGARDE_H

#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QJsonArray>


class Sauvegarde
{
    public:
        Sauvegarde();

        QJsonObject Charger(const QString& fichier);
        void Sauvegarder( QJsonObject obj, const QString& fichier);
};

#endif // SAUVEGARDE_H
