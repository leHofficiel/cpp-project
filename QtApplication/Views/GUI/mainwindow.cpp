#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "qpushbutton.h"
#include "qlabel.h"
#include "qgridlayout.h"
#include <vector>
#include "QVBoxLayout"
#include "qlayoutitem.h"
#include <QTime>
#include <QWidget>
#include <QLayoutItem>
#include <QLayout>
#include <QFileDialog>
#include "qlineedit.h"
#include "../../Controllers/GameController.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->gridLayout->setHorizontalSpacing(0);
    ui->gridLayout->setVerticalSpacing(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::DisplayStartMenu() const {
    ui->stackedWidget->setCurrentIndex(START_MENU_PAGE_INDEX);
    DisplayMainMessage("Yo Paul Launcher");
}

void MainWindow::DisplayGameMenu() const {      
    ui->stackedWidget->setCurrentIndex(GAME_MENU_PAGE_INDEX);
     DisplayMainMessage("Selection du jeu");
}

void MainWindow::DisplayPlayerMenu(bool isBotEnable) const {
    ui->stackedWidget->setCurrentIndex(PLAYER_MENU_PAGE_INDEX);
    DisplayMainMessage("Joueurs");
    if(isBotEnable){
        ui->checkBox->show();
        ui->checkBox_2->show();
    }
    else{
        ui->checkBox->hide();
        ui->checkBox_2->hide();
    }
}

void MainWindow::DisplayGridView(int lines, int columns) const {
    ui->stackedWidget->setCurrentIndex(GRID_PAGE_INDEX);
    ui->nextbtn->hide();
    ui->quitbtn->hide();
    ClearGridLayout();
    for(int  i = 0;i<lines;i++)
    {
        for(int j = 0;j<columns; j++)
        {                        
            QPushButton* button = new QPushButton();
            button->setMinimumSize(130-lines*10,125-columns*10);
            ui->gridLayout->addWidget(button,i,j);
            button->setStyleSheet("background-color: rgb(114,117,126); font-size:16pt;");
            QFont font = button->font();
            font.setPointSize(14);
            button->setFont(font);
            connect(button , &QPushButton::clicked, [=](){                
                emit GameController::GetInstance()->PlayerInput_Signal(Position(i,j));
            });
        }
    }    
}
void MainWindow::DisplayFinalState(std::string label) const {    
    DisplayMainMessage(label);
    ui->nextbtn->show();
    ui->quitbtn->show();
}

void MainWindow::DisplayEndMenu() const {    
    ui->stackedWidget->setCurrentIndex(END_MENU_PAGE_INDEX);
}


void MainWindow::DisplayRequestInputByXY(int width, int height) const {}
void MainWindow::DisplayRequestInputByCols(int width) const {}
void MainWindow::DisplayRequestInputByLine(int height) const {}



void MainWindow::DisplayMainMessage(std::string message) const{
    ui->yoPaulLbl->setText(QString::fromStdString(message));
}

void MainWindow::DisplayMessage(std::string message) const{
    ui->indicationlbl->setText(QString::fromStdString(message));
    ui->indication2lbl->hide();
}

void MainWindow::DisplayWarningMessage(std::string message) const {
    ui->indication2lbl->setText(QString::fromStdString(message));
    ui->indication2lbl->show();
}

void MainWindow::DisplayGrid(const Grid& grid) {
    int lines = grid.GetLineNumber();
    int columns = grid.GetColumnNumber();    

    for(int  i = 0;i<lines;i++)
    {
       for(int j = 0;j<columns; j++)
       {
            std::string cellValue;
            cellValue.push_back(grid.GetCell({i,j}));
            QString qCellValue = QString::fromStdString(cellValue);

            QPushButton* button = dynamic_cast<QPushButton*>(ui->gridLayout->itemAtPosition(i, j)->widget());
            button->setText(qCellValue);

            if(cellValue=="x")
            {
                button->setStyleSheet("background-color: rgb(204,255,255)");
            }
            else if(cellValue=="y")
            {
                button->setStyleSheet("background-color: rgb(255,204,204)");
            }
            else if(cellValue=="X")
            {
                button->setStyleSheet("background-color: rgb(102,178,255)");
            }
            else if(cellValue=="Y")
            {
                button->setStyleSheet("background-color: rgb(255,102,102)");
            }
            else
            {
                button->setStyleSheet("background-color: rgb(114,117,126)");
            }
       }
   }
}


void MainWindow::ClearGridLayout() const {
    while(ui->gridLayout->count() != 0)
    {
        QLayoutItem* item = ui->gridLayout->itemAt(0);
            ui->gridLayout->removeItem(item);
            QWidget* widget = item->widget();

            if(widget)
            {
                delete widget;
            }
    }   
}


void MainWindow::OpenFileForLoad(){
    QFileDialog fd(this);
    fd.setNameFilter(tr("Json (*.json)"));
    QString file;
    if(fd.exec())
    {
        file = fd.selectedFiles()[0];
        emit GameController::GetInstance()->LoadGame_Signal(file.toStdString());
    }
}

//void MainWindow::QDosDane(bool &bill) const{
//    while(!bill)
//    {

//    }
//}


void MainWindow::on_quitbtn_clicked()
{
    emit GameController::GetInstance()->Exit_Signal();
}

void MainWindow::on_nouvellepartiebtn_clicked()
{
    emit GameController::GetInstance()->CreateNewGame_Signal();
}


void MainWindow::on_morpionbtn_clicked()
{
    emit GameController::GetInstance()->GameChosed_Signal(0);
}


void MainWindow::on_puissance4btn_clicked()
{
    emit GameController::GetInstance()->GameChosed_Signal(1);
}


void MainWindow::on_othellobtn_clicked()
{
    emit GameController::GetInstance()->GameChosed_Signal(2);
}


void MainWindow::on_ladiesbtn_clicked()
{
    emit GameController::GetInstance()->GameChosed_Signal(3);
}


void MainWindow::on_pushButton_2_clicked()
{
    std::string namePlayer1 = ui->joueur1le->text().toStdString();
    std::string namePlayer2 = ui->joueur2le->text().toStdString();
     if (namePlayer1=="")
     {
        namePlayer1=ui->joueur1le->placeholderText().toStdString();
     }
     if (namePlayer2=="")
     {
        namePlayer2=ui->joueur2le->placeholderText().toStdString();
     }
    bool isBotPlayer1 = ui->checkBox->isChecked();
    bool isBotPlayer2 = ui->checkBox_2->isChecked();
    emit GameController::GetInstance()->CreatePlayer_Signal(namePlayer1, isBotPlayer1, namePlayer2, isBotPlayer2);
}


void MainWindow::on_chargerpartiebtn_clicked()
{
    OpenFileForLoad();
}


void MainWindow::on_nextbtn_clicked()
{
    emit GameController::GetInstance()->GoToEndPage_Signal();
}


void MainWindow::on_rjouerbtn_clicked()
{
    emit GameController::GetInstance()->Replay_Signal();
}


void MainWindow::on_rejouer2btn_clicked()
{
    emit GameController::GetInstance()->ReplayWithNewPlayers_Signal();
}


void MainWindow::on_autrejeubtn_clicked()
{
    emit GameController::GetInstance()->CreateNewGame_Signal();
}


void MainWindow::on_quitterbtn_clicked()
{    
    OpenFileForLoad();
}


void MainWindow::on_savebtn_clicked()
{    
    QFileDialog fd(this);
    QString file;
    if(fd.exec())
    {
        file = fd.selectedFiles()[0];
        emit GameController::GetInstance()->Save_Signal(file.toStdString());
    }
}

