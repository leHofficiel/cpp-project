#include <random>
#include <functional>
#include "../../include/Controllers/Othello.h"
#include "../../include/Controllers/GameController.h"

//
// PUBLIC
//

Othello::Othello(){
    std::cout << "Game mode = Othello" << std::endl << std::endl;
}

int Othello::GetGridLineNumber() const {return GRID_LINES;}
int Othello::GetGridColumnNumber() const {return GRID_COLUMNS;}
int Othello::GetPlayerNumber() const {return PLAYER_NUMBER;}


void Othello::Play( GameController* ctrl) const {
    std::shared_ptr<UserInterface> UserInterface = ctrl->GetUserInterface();    
    Player player = ctrl->GetCurrentPlayer();
    char enemyChar = ctrl->GetOtherPlayer(player).GetSymbol();

    Grid& grid = ctrl->GetGrid();    

    bool isCorrect = false;
    Position inputPosition;
    do{ 
        inputPosition = UserInterface->GetInputByXY(GRID_COLUMNS, GRID_LINES);
        if(!grid.IsEmpty(inputPosition)){ 
            UserInterface->DisplayMessage("Placement Impossible\n");            
        }
        else {             
            if( EstSandwichVertical(grid, inputPosition, player.GetSymbol(),enemyChar) +
                EstSandwichHorizontal(grid, inputPosition, player.GetSymbol(), enemyChar) + 
                EstSandwichDiagonal(grid, inputPosition, player.GetSymbol(), enemyChar) ){
                isCorrect = true;
            }
            else{
                UserInterface->DisplayMessage("Placement Incorrect\n");
            }
        }        
    }while(!isCorrect);

    grid.Place(inputPosition, player.GetSymbol());
    
}


void Othello::AIPlay( GameController* ctrl) const {
    std::random_device r;
    std::vector<Position> emptyCells = ctrl->GetGrid().GetEmptyCells();
    auto gen = std::bind(std::uniform_int_distribution<>(0,emptyCells.size()),std::default_random_engine(r()));
    Position pos;
    bool isCorrect = false;
    Player player = ctrl->GetCurrentPlayer();
    while(!isCorrect){
        pos = emptyCells[gen()];
        auto neighs = ctrl->GetGrid().GetNeighbors(pos,player.GetSymbol());
        for(auto neigh: neighs){
            if(neigh != player.GetSymbol() && neigh != ' '){
                isCorrect = true;
                break;
            }
        }
    }

    ctrl->GetGrid().Place(pos, player.GetSymbol());    
}

void Othello::GridInitState(GameController* ctrl) const {
    Grid& grid = ctrl->GetGrid();
    grid.Set(Position(3,3), ctrl->GetPlayers()[0].GetSymbol());
    grid.Set(Position(4,4), ctrl->GetPlayers()[0].GetSymbol());
    
    grid.Set(Position(4,3), ctrl->GetPlayers()[1].GetSymbol());
    grid.Set(Position(3,4), ctrl->GetPlayers()[1].GetSymbol());    
}


GameState Othello::GridState(GameController* ctrl) const {
    const Grid grid = ctrl->GetGrid();
    char symbol = ctrl->GetCurrentPlayer().GetSymbol();
    char enemy = ctrl->GetOtherPlayer(ctrl->GetCurrentPlayer()).GetSymbol();
    if(Win(grid, symbol, enemy)){
        return GameState::WIN;
    }
    else if(Equality(grid,symbol)){
        return GameState::EQUALITY;
    }
    return GameState::NOTHING;
}

//
// PRIVATE
//

bool Othello::Win(const Grid &grid, char symbol, char enemy) const {
    if (!grid.GetEmptyCells().empty()) return false;
    if (grid.GetSymbolCount(symbol) < (grid.GetColumnNumber() * grid.GetLineNumber())/2) return false;

    int compteur=0;
    int compteur2=0;
    for(int i = 0; i < grid.GetLineNumber(); i++){
        for(int j = 0; j < grid.GetColumnNumber(); j++){
            if(grid.GetCell({i,j}) == symbol){
                compteur++;
            }
        }
    }
    for(int i = 0; i < grid.GetLineNumber(); i++){
        for(int j = 0; j < grid.GetColumnNumber(); j++){
            if(grid.GetCell({i,j}) == enemy){
                compteur2++;
            }
        }
    }

    if(compteur < compteur2){
        return false;
    }

    return true;
}

bool Othello::Equality(const Grid &grid, char symbol) const {
    return grid.GetSymbolCount(symbol) == (grid.GetColumnNumber() * grid.GetLineNumber())/2;
}

typedef void* Xx__getComputeQuantumBestPositionByXYFromCircularPain__xX;
typedef void* BonSoleil;
typedef GameController* LeMondeEstMechant;


// IsWhitePain
bool Othello::EstSandwichVertical(Grid& grid, Position &position, char player, char enemy) const{
    Position position2;
    int line = position.line;
    int column = position.column;
    bool sandwich = false;
    int i;

    // EN HAUT
    if(line != 0) 
    {
        i = line-1; 
        if(grid.GetCell({i, column}) == enemy)
        {
            while(i > 0 && grid.GetCell({i,column}) == enemy && !grid.IsEmpty({i, column}))
            {
                i--;
            } 
            if(grid.GetCell({i,column}) == player)
            {
                sandwich = true;
                RetournementVertical(grid, column, i, line, player);
            }
        }
    }

    // EN BAS
    if(column != GRID_COLUMNS-1 && line !=GRID_LINES-1)
    { 
        i = line+1;
        if(grid.GetCell({i,column}) == enemy)
        {
            while(i < GRID_COLUMNS-1 && grid.GetCell({i,column}) != player && !grid.IsEmpty({i,column}))
            {
                i++;
            }
            if(grid.GetCell({i,column}) == player)
            {
                sandwich = true;
                RetournementVertical(grid, column, line, i, player);
            }
        } 
    }
    return sandwich;

}
bool Othello::EstSandwichHorizontal(Grid& grid, Position &position, char player, char enemy) const{ 
    int x = position.line;
    int y = position.column;
    bool sandwich = false;
    int j; 
    // A GAUCHE
    if(y != 0)
    {
        j=y-1;
        if(grid.GetCell({x,j}) == enemy)
        {
            while(j >= 0 && grid.GetCell({x,j}) == enemy && !grid.IsEmpty({x,j}))
            {
                j--;
            }
            if(grid.GetCell({x,j}) == player)
            {
                sandwich = true;
                RetournementHorizontal(grid, x, j, y, player);
            }
        }
    }
     // A DROITE
    if(y != GRID_COLUMNS-1)
    {
        j=y+1;
        if(grid.GetCell({x,j}) == enemy)
        {
            while(j < GRID_COLUMNS-1 && grid.GetCell({x,j}) != player && !grid.IsEmpty({x,j}))
            {
                j++;
            }
            if(grid.GetCell({x,j}) == player)
            {
                sandwich = true;
                RetournementHorizontal(grid, x, y, j, player);
            }
        } 
    }
    return sandwich;

}
bool Othello::EstSandwichDiagonal(Grid& grid, Position &position, char player, char enemy) const{
    int line = position.line;
    int column = position.column;

    bool sandwich = false;
    int i;
    int j;

    // EN HAUT A GAUCHE
    if(line != 0 && column != 0)
    {
        i=line-1;
        j=column-1;

        if(grid.GetCell({i,j}) == enemy)
        {
            while(i > 0 && j > 0 && grid.GetCell({i,j}) == enemy && !grid.IsEmpty({i,j}))
            {
                i--;
                j--;
            }
            if(grid.GetCell({i,j}) == player)
            {
                sandwich = true;
                RetournementDiagonal(grid, i, j, line, column, player,1);
            }
        }
    }

    // EN HAUT A DROITE
    if(line != 0 && column != GRID_COLUMNS-1)
    {
        i=line-1;
        j=column+1;

        if(grid.GetCell({i,j}) == enemy)
        {
            while(i > 0 && j < GRID_COLUMNS-1 && grid.GetCell({i,j}) == enemy && !grid.IsEmpty({i,j}))
            {
                i--;
                j++;
            }
            if(grid.GetCell({i,j}) == player)
            {
                sandwich = true;
                RetournementDiagonal(grid, i, j, line, column, player, 2);
            }
        }
    }
    // EN BAS A GAUCHE
    if(line != GRID_LINES-1 && column != 0)
    {
        i=line+1;
        j=column-1;

        if(grid.GetCell({i,j}) == enemy)
        {
            while(i < GRID_LINES-1 && j > 0 && grid.GetCell({i,j}) == enemy && !grid.IsEmpty({i,j}))
            {
                i++;
                j--;
            }
            if(grid.GetCell({i,j}) == player)
            {
                sandwich = true;
                RetournementDiagonal(grid, line, column, i, j, player, 2);
            }
        }
    }

    // EN BAS A DROITE
    if(line != GRID_COLUMNS-1 && column != GRID_COLUMNS-1)
    {
        i=line+1;
        j=column+1;

        if(grid.GetCell({i,j}) == enemy)
        { 
            while(i < GRID_LINES-1 && j < GRID_COLUMNS-1 && grid.GetCell({i,j}) == enemy && !grid.IsEmpty({i,j}))
            {
                i++;
                j++;
            }
            if(grid.GetCell({i,j}) == player)
            {
                sandwich = true;
                RetournementDiagonal(grid, line, column, i, j, player,1);
            }
        }
    }
    return sandwich;

}

void Othello::RetournementVertical(Grid& grid, int column, int line, int line2, char player) const{
    for(int j = line; j < line2; j++)
    {
        grid.Set({j,column}, player);        
    }

}
void Othello::RetournementHorizontal(Grid& grid, int line, int column, int column2, char player) const{ 
    for(int j = column; j < column2; j++)
    {        
        grid.Set({line, j}, player);
    }
}
void Othello::RetournementDiagonal(Grid& grid, int line, int column, int line2, int column2, char player, int diagonal) const{
    if(diagonal == 1)
    {
        for(int i = line, j = column; i < line2 && j < column2; i++, j++)
        {            
            grid.Set({i,j}, player);
        }
    }   
    else
    {
        for(int i = line, j = column; i <= line2 && j >= column2; i++, j--)
        {            
            grid.Set({i,j}, player);
        }
    }
}



