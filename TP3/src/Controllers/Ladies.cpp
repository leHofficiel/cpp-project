#include "Controllers/Ladies.h"
#include "../../include/Controllers/GameController.h"


//
// PUBLIC
// 

Ladies::Ladies() {
    std::cout << "Game mode = Ladies" << std::endl << std::endl;
}

void Ladies::GridInitState(GameController *ctrl) const {
    Grid& grid = ctrl->GetGrid();

    // initialiser la grille de dame
    for(int i=0;i<grid.GetLineNumber();i++){
        for(int j=0;j<grid.GetColumnNumber();j++){
            // de la ligne 0 à la ligne 3 placer les pions X quand c'est pair
            // de la ligne 6 à la ligne 9 placer les pions O quand c'est pair
            if(i<4 && i%2!=0 && j%2==0){
                grid.Place(Position(i,j), ctrl->GetPlayers()[0].GetSymbol());
            }
            else if(i<4 && i%2==0 && j%2!=0){
                grid.Place(Position(i,j), ctrl->GetPlayers()[0].GetSymbol());
            }
            else if(i>5 && i%2!=0 && j%2==0){
                grid.Place(Position(i,j), ctrl->GetPlayers()[1].GetSymbol());
            }
            else if(i>5 && i%2==0 && j%2!=0){
                grid.Place(Position(i,j), ctrl->GetPlayers()[1].GetSymbol());
            }
        }
    }
}

void Ladies::Play(GameController *ctrl) const {
    Grid& grid = ctrl->GetGrid();
    Position base_pos = ctrl->GetUserInterface()->GetInputByXY(GRID_COLUMNS, GRID_LINES);

    while(tolower(grid.GetCell(base_pos)) != (ctrl->GetCurrentPlayer().GetSymbol())){
        base_pos = ctrl->GetUserInterface()->GetInputByXY(GRID_COLUMNS, GRID_LINES);
    }

    if(grid.isSymbolSpecial(base_pos)){
        SpecialPlay(ctrl,base_pos);
    }
    else NormalPlay(ctrl,base_pos);
}

void Ladies::AIPlay(GameController* ctrl) const {
    
}

GameState Ladies::GridState(GameController* ctrl) const {
    if(ctrl->GetGrid().GetSymbolCount(ctrl->GetCurrentPlayer().GetSymbol()) == 0) return GameState::DEFEAT;
    return GameState::NOTHING;
}


//
// Private
//

bool Ladies::CanAttack(GameController *ctrl) const {
    Grid& grid = ctrl->GetGrid();
    auto pions = ctrl->GetGrid().GetAllPositionBySymbol(ctrl->GetCurrentPlayer().GetSymbol());
    for(auto& neighbor : pions){
        if(grid.GetCell(neighbor) != ctrl->GetCurrentPlayer().GetSymbol() && grid.GetCell(neighbor) != ' '){ // si adversaire
            auto adv_neigh = (ctrl->IsFirst(ctrl->GetCurrentPlayer())) ? grid.GetDownNeighborsPosition(neighbor) : grid.GetUpNeighborsPosition(neighbor);
            // adding to known neighbors
            for (auto& adv: adv_neigh){
                if(grid.GetCell(adv) == ' '){
                    pions.push_back(adv);
                    return true;
                }
            }
        }
    }
    return false;
}

bool Ladies::CheckIsValidPosition(const Position &pos) const{
    unsigned int line = pos.line;
    unsigned int column = pos.column;

    return ((line%2!=0 && column%2==0) || (line%2==0 && column%2!=0));
}

bool Ladies::Win(const Grid &grid, char symbol) const {
    return false;
}

bool Ladies::Equality(const Grid &grid, char symbol) const {
    return false;
}

void Ladies::DelDouble(std::vector<Position> & v) const{
    for(int i = 0 ; i < v.size(); i++){
        auto cur = v[i];
        for(int j = i; j < v.size() ; j++){
            if(Position::isEqual(cur,v[j])){
                v.erase(v.begin() + j);
                continue;
            }
        }
    }
}

enum relatives {
    UPL,
    UPR,
    DOWNL,
    DOWNR
};

relatives GetRelativesDirections(Position a, Position target){
    auto relative = Position(target.line - a.line, target.column - a.column);
    if(relative.line < 0 && relative.column > 0) return UPR;
    else if(relative.line < 0 && relative.column < 0) return UPL;
    else if(relative.line > 0 && relative.column > 0) return DOWNR;
    else if(relative.line > 0 && relative.column < 0) return DOWNL;
}

void Ladies::NormalPlay(GameController *ctrl, Position base_pos) const {
    Grid& grid = ctrl->GetGrid();

    redo:
    std::vector<std::pair<Position,Position>> neighs = std::vector<std::pair<Position,Position>>();
    auto base_neigh = (ctrl->IsFirst(ctrl->GetCurrentPlayer())) ? grid.GetDownNeighborsPosition(base_pos) : grid.GetUpNeighborsPosition(base_pos);
    bool canAttack = false;

    printf("--\n");
    // check for adversary
    for(auto& neighbor : base_neigh){
        if(grid.GetCell(neighbor) != ctrl->GetCurrentPlayer().GetSymbol() && grid.GetCell(neighbor) != ' '){ // si adversaire
            Position target_pos;
            auto relative = GetRelativesDirections(base_pos,neighbor);
            switch (relative) {
                case UPL:
                    target_pos = grid.GetUpLeftPosition(neighbor);
                    break;
                case UPR:
                    target_pos = grid.GetUpRightPosition(neighbor);
                    break;
                case DOWNL:
                    target_pos = grid.GetDownLeftPosition(neighbor);
                    break;
                case DOWNR:
                    target_pos = grid.GetDownRightPosition(neighbor);
                    break;
            }
            // adding to known neighbors
            if(grid.GetCell(target_pos) == ' '){
                neighs.emplace_back(target_pos, neighbor);
                canAttack = true;
                printf("Can attack\n");
            }
        }
    }
    // if no attack possible
    if(!canAttack){
        for(auto& neighbor : base_neigh){
            if(grid.GetCell(neighbor) == ' ')
                neighs.emplace_back(neighbor,Position(666,666));
        }
    }

    for(auto& neigh: neighs){
        auto relative = GetRelativesDirections(base_pos,neigh.first);
        printf("{%d,%d} // %d\n",neigh.first.line,neigh.first.column,relative);
    }

    auto check = [&](Position a) {
        for(auto& other : neighs){
            if( other.first.column == a.column && other.first.line == a.line ){
                return true;
            }
        }
        return false;
    };
    auto GetVictim = [&](Position a) {
        for(auto& other : neighs){
            if( other.first.column == a.column && other.first.line == a.line ){
                return other.second;
            }
        }
        return Position(666,666);
    };

    // request next pos
    Position next_pos = ctrl->GetUserInterface()->GetInputByXY(GRID_COLUMNS, GRID_LINES);
    printf("check : %d\n",!check(next_pos));

    printf("grid : %d\n",grid.GetCell(next_pos) != ' ');
    while(grid.GetCell(next_pos) != ' ' || !check(next_pos)){
        printf("not valid\n");
        next_pos = ctrl->GetUserInterface()->GetInputByXY(GRID_COLUMNS, GRID_LINES);
    }

    grid.Set(next_pos,ctrl->GetCurrentPlayer().GetSymbol());
    grid.Set(base_pos,' ');

    auto victim = GetVictim(next_pos);
    if(canAttack && victim.line != 666){
        grid.Set(victim,' ');
    }

    // transform in a distinguished lady
    if(ctrl->IsFirst(ctrl->GetCurrentPlayer())){
        if(next_pos.line == ctrl->GetGrid().GetLineNumber() - 1){
            grid.ToSpecialSymbol(next_pos);
        }
    }
    else {
        if(next_pos.line == 0){
            grid.ToSpecialSymbol(next_pos);
        }
    }

    if(this->CanAttack(ctrl) && neighs.size() > 1 && canAttack) {
        printf("canAttack :%d\n",this->CanAttack(ctrl));
        goto redo;
    }
}

void discardTail(const Grid& grid, std::vector<Position>& list, char sym){
   for(int i = 0; i < list.size();i++){
        if(grid.GetCell(list[i]) == sym){
            for(int j = 0; j < list.size(); j ++){
                list.erase(list.begin() + j);
            }
            break;
        }
   }
}

void Ladies::SpecialPlay(GameController *ctrl, Position base_pos) const {
    Grid& grid = ctrl->GetGrid();

    redo:
    std::vector<std::pair<Position,Position>> neighs = std::vector<std::pair<Position,Position>>();
    auto ascendleft = grid.GetNeighPosFromAscendingDiagonalLeft(base_pos);
    discardTail(grid, ascendleft, ctrl->GetCurrentPlayer().GetSymbol());
    auto ascendright = grid.GetNeighPosFromAscendingDiagonalRight(base_pos);
    discardTail(grid, ascendright, ctrl->GetCurrentPlayer().GetSymbol());
    auto descendleft = grid.GetNeighPosFromDescendingDiagonalLeft(base_pos);
    discardTail(grid, descendleft, ctrl->GetCurrentPlayer().GetSymbol());
    auto descendright = grid.GetNeighPosFromDescendingDiagonalRight(base_pos);
    discardTail(grid, descendright, ctrl->GetCurrentPlayer().GetSymbol());

    std::vector<Position> base_neigh;
    base_neigh.insert(base_neigh.end(),ascendleft.begin(),ascendleft.end());
    base_neigh.insert(base_neigh.end(),ascendright.begin(),ascendright.end());
    base_neigh.insert(base_neigh.end(),descendleft.begin(),descendleft.end());
    base_neigh.insert(base_neigh.end(),descendright.begin(),descendright.end());
    bool canAttack = false;

    printf("--\n");
    for(auto& neigh: base_neigh){
        printf("{%d,%d}\n",neigh.line,neigh.column);
    }
    printf("--\n");

    for(auto& neighbor : base_neigh){
        if(grid.GetCell(neighbor) != ctrl->GetCurrentPlayer().GetSymbol() && grid.GetCell(neighbor) != ' '){ // si adversaire
            Position target_pos;
            auto relative = GetRelativesDirections(base_pos,neighbor);
            switch (relative) {
                case UPL:
                    target_pos = grid.GetUpLeftPosition(neighbor);
                    break;
                case UPR:
                    target_pos = grid.GetUpRightPosition(neighbor);
                    break;
                case DOWNL:
                    target_pos = grid.GetDownLeftPosition(neighbor);
                    break;
                case DOWNR:
                    target_pos = grid.GetDownRightPosition(neighbor);
                    break;
            }
            // adding to known neighbors
            if(grid.GetCell(target_pos) == ' '){
                neighs.emplace_back(target_pos, neighbor);
                canAttack = true;
                printf("Can attack\n");
            }
        }
    }
    // if no attack possible
    if(!canAttack){
        for(auto& neighbor : base_neigh){
            if(grid.GetCell(neighbor) == ' ')
                neighs.emplace_back(neighbor,Position(666,666));
        }
    }
    for(auto& neigh: neighs){
        auto relative = GetRelativesDirections(base_pos,neigh.first);
        printf("{%d,%d} // %d\n",neigh.first.line,neigh.first.column,relative);
    }

    auto check = [&](Position a) {
        for(auto& other : neighs){
            if( other.first.column == a.column && other.first.line == a.line ){
                return true;
            }
        }
        return false;
    };
    auto GetVictim = [&](Position a) {
        for(auto& other : neighs){
            if( other.first.column == a.column && other.first.line == a.line ){
                return other.second;
            }
        }
        return Position(666,666);
    };

    // request next pos
    Position next_pos = ctrl->GetUserInterface()->GetInputByXY(GRID_COLUMNS, GRID_LINES);
    printf("check : %d\n",!check(next_pos));

    printf("grid : %d\n",grid.GetCell(next_pos) != ' ');
    while(grid.GetCell(next_pos) != ' ' || !check(next_pos)){
        printf("not valid\n");
        next_pos = ctrl->GetUserInterface()->GetInputByXY(GRID_COLUMNS, GRID_LINES);
    }

    grid.Set(next_pos, toupper(ctrl->GetCurrentPlayer().GetSymbol()));
    grid.Set(base_pos,' ');

    auto victim = GetVictim(next_pos);
    if(canAttack && victim.line != 666){
        grid.Set(victim,' ');
    }

    if(this->CanAttack(ctrl) && neighs.size() > 1 && canAttack) {
        printf("canAttack :%d\n",this->CanAttack(ctrl));
        goto redo;
    }
}
