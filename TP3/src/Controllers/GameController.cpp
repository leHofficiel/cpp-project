#include "../../include/Controllers/GameController.h"
#include <memory>
#include <utility>

//
// CONSTRUCTOR
//

GameController::GameController() {
    isInit = false;
}

//
// PUBLIC
//

Player GameController::GetOtherPlayer(const Player& player) const {
    for(Player other : players){
        if( other.GetSymbol() != player.GetSymbol() ){
            return other;
        }
    }
    return {};
}

void GameController::Play(){
    this->grid.Clear();
    if(!isInit){
        Init();
    }
    bool run = true;
    while (run)
    {        
        for(const auto& player: this->players){            
            PlayRound();            

            if(grid.GetEmptyCells().empty()){
                FindWinner();
                run = false;
            }
            else run = FindGameState(player);

            if(!run) break;

            this->currentPlayerIndex = (this->currentPlayerIndex+1)%players.size();            
        }
    }
    userInterface->DisplayGrid(this->grid);
    userInterface->ClearInterface();


    int choice = userInterface->DisplayChoiceList(END_GAME_ACTION_LIST);
    userInterface->ClearInterface();
    switch(choice){
        case 0:
            Play(); break;
        case 1:
            CreateNewPlayers();
            Play(); break;
        case 2:            
            RestartGame();
        default:
            break;
    }
}

void GameController::RestartGame(){
    userInterface->ClearInterface();
    isInit = false;
    Play();
}

void GameController::SaveGameState(std::string path)
{    
    Sauvegarde().Sauvegarder(toJson(), QString::fromStdString(path));
}

//
// PRIVATE
//

void GameController::Init(){

    int choice = userInterface->DisplayChoiceList(START_GAME_ACTION_LIST);
    if(choice == 0){
        InitNewGame();
    }
    else{
        LoadGame();
    }
    isInit = true;
}

void GameController::InitNewGame(){    
    // Select game mode
    SelectGameMode(); 
    // Create players
    CreateNewPlayers();
    // Init grid
    this->grid = Grid(this->gameType->GetGridLineNumber(), this->gameType->GetGridColumnNumber());
    gameType->GridInitState(this);

    this->currentPlayerIndex=0;
}

void GameController::LoadGame(){
    std::string file = userInterface->GetUserFile("path");
    
    QJsonObject obj = Sauvegarde().Charger(QString::fromStdString(file));    
    
    // Select game mode
    InitGameMode(obj.value("GameType").toInt());    
    // Create players
    LoadPlayers(obj.value("Players").toArray());    
    // Init grid
    LoadGrid(obj.value("Grid").toArray());    

    this->currentPlayerIndex = obj.value("CurrentPlayerIndex").toInt();
}


// NEW GAME FUNCTIONS
void GameController::SelectGameMode(){
    int choice = userInterface->DisplayChoiceList(GAME_CHOICE_LIST);    
    InitGameMode(choice);
}
void GameController::CreateNewPlayers(){


    this->players.clear();
    std::string name;
    bool isBot;
    for(int i=0; i<this->gameType->GetPlayerNumber(); i++){
        isBot = userInterface->DisplayChoiceList(PLAYER_CREATION_ACTION_LIST) == 1;        
        name = userInterface->GetUserStringInput("nom");
        CreatePlayer(name, syms[i], isBot);
    }   
}

// LOAD GAME FUNCTIONS
void GameController::LoadPlayers(QJsonArray players)
{    
    for (int i = 0; i < players.size(); i++)
    {
        QJsonObject player = players.at(i).toObject();
        std::string name = player.value("name").toString().toStdString();
        std::string symbol = player.value("symbol").toString().toStdString();
        bool isBot = player.value("isBot").toBool();
        CreatePlayer(name, symbol[0], isBot);
    }
}
void GameController::LoadGrid(QJsonArray qGrid)
{
    this->grid = Grid(this->gameType->GetGridLineNumber(), this->gameType->GetGridColumnNumber());    
    for(int i=0; i< grid.GetColumnNumber()*grid.GetLineNumber(); i++ ){
        QJsonObject cell = qGrid.at(i).toObject();
        std::string symbol = cell.value("symbol").toString().toStdString();
        this->grid.Set(i, symbol[0]);
    }  
}


// COMMON FUNCTIONS
void GameController::InitGameMode(int gameId){
    this->gameId = gameId;
    switch (gameId)
    {
        case 0:
            gameType = std::make_shared<Morpion>();
            break;
        case 1:
            gameType = std::make_shared<Puissance4>();
            break;
        case 2:
            gameType = std::make_shared<Othello>();
            break;
        case 3:
            gameType = std::make_shared<Ladies>();
            break;
    }
}
void GameController::CreatePlayer(std::string name, char symbol, bool isBot){
    players.push_back(Player(name, symbol, isBot));
}




void GameController::PlayRound(){
    
    userInterface->DisplayGrid(this->grid);
    userInterface->DisplayCurrentPlayer(GetCurrentPlayer());            
    
    if(GetCurrentPlayer().IsBot()){
        gameType->AIPlay(this);
    }
    else{
        gameType->Play(this);
    }
}

void GameController::FindWinner(){
    for(int i=0; i<players.size(); i++){
        this->currentPlayerIndex = i;
        if(gameType->GridState(this) == GameState::WIN){
            userInterface->DisplayWinnerMessage(players[i]);
            return;
        }
    }
    userInterface->DisplayEqualityMessage();
}

bool GameController::FindGameState(const Player& player){
    bool result = true;
    switch (gameType->GridState(this)){
        case GameState::WIN:
            userInterface->DisplayWinnerMessage(player);
            result = false;
            break;
        case GameState::EQUALITY:
            userInterface->DisplayEqualityMessage();
            result = false;
            break;
        case GameState::NOTHING:
            break;
        case DEFEAT:
            auto p = GetOtherPlayer(player);
            userInterface->DisplayWinnerMessage(p);
            break;
    }
    return result;
}



QJsonObject GameController::toJson()
{
    QJsonObject json;    
    json.insert("GameType", gameId);
    json.insert("CurrentPlayerIndex", currentPlayerIndex);

    QJsonObject playersJson;
    QJsonArray playersArray;
    for (const auto& player : players)
    {
        QJsonObject playerJson;
        playerJson["name"] = QJsonValue( QString::fromStdString(player.GetName()) ) ;
        playerJson["symbol"] = QJsonValue( QString(player.GetSymbol()) ) ;
        playerJson["isBot"] =  QJsonValue( player.IsBot() ) ;
        playersArray.append(playerJson);
    }
    json.insert("Players", playersArray);

    QJsonObject gridJson;
    QJsonArray gridArray;
    for(int i=0; i<grid.GetLineNumber()*grid.GetColumnNumber(); i++){
        QJsonObject cell;
        cell["symbol"] = QJsonValue(QString(grid.GetCell(i)));
        gridArray.append(cell);
    }  
    json.insert("Grid", gridArray);    

    return json;    
}




