#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "qpushbutton.h"
#include "qlabel.h"
#include "qgridlayout.h"
#include <vector>
#include "QVBoxLayout"
#include "qlayoutitem.h"
#include <QTime>
#include <QWidget>
#include <QLayoutItem>
#include <QLayout>
#include <QFileDialog>
#include "qlineedit.h"


std::vector <QPushButton*> buttons;
bool bill2=false;
Position position;
int res;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

Position MainWindow::GetInputByXY(int width, int height) const {
    bill2 = false;
    while (!bill2)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

    return position;
}

Position MainWindow::GetInputByCols(int width) const {
    bill2 = false;
    while (!bill2)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

    position.line = 0;
    return position;

}
void MainWindow::RegisterController(GameController* ctrl)
{
    this->gamectrl=ctrl;
}

std::string MainWindow::GetUserFile(std::string label)
{
    QFileDialog fd(this);
    //fd.setNameFilter();
    QString file;
    if(fd.exec())
    {
        file = fd.selectedFiles()[0];

        return file.toStdString();
    }
}

Position MainWindow::GetInputByLine(int height) const {
    bill2 = false;
    while (!bill2)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

    position.column = 0;
    return position;
}

std::string MainWindow::GetUserStringInput(std::string label)
{
    bill2=false;
    ClearInterface();
    QLabel *nom= new QLabel();
    nom->setText("Nom du joueur : ");
    gridLayout->addWidget(nom,0,0);
    QLineEdit * nomtxt= new QLineEdit();
    gridLayout->addWidget(nomtxt,0,1);
    gridLayout->setHorizontalSpacing(30);
    QPushButton *valider=new QPushButton("Valider");
    gridLayout->addWidget(valider,0,2);
    connect(valider, &QPushButton::clicked, [=](){bill2=true;});
    while (!bill2)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    std::string temp=nomtxt->text().toStdString();
    ClearInterface();
    return temp;
}



int MainWindow:: DisplayChoiceList(std::vector<std::string> choiceList)  {

    bill2=false;

    QCoreApplication::processEvents();
    int index=0;
    gridLayout = std::make_shared<QGridLayout>();

    gridLayout->setHorizontalSpacing(0);
    gridLayout->setVerticalSpacing(0);
    this->centralWidget()->setLayout(gridLayout.get());

    ClearInterface();

    for (std::string mode : choiceList )
    {
        QPushButton *button = new QPushButton();
        QString merde= QString::fromStdString(mode);
        button->setText(merde);
        button->setMinimumSize(75,75);
        gridLayout->setContentsMargins(150,0,150,0);
        gridLayout->addWidget(button);
        connect(button, &QPushButton::clicked, [=](){bill2=true;res=index;});
        index++;
    }

    while (!bill2)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

//    Ghetto LIQID
//    if(choiceList.at(0)=="Morpion")
//    {
//        gameMode=res;
//    }

    ClearInterface();
    return res;
}




void MainWindow::DisplayMessage(std::string message) const {
    QString merde= QString::fromStdString(message);
    ui->yoHamzalbl->setText(merde);
}

void MainWindow::DisplayGrid(const Grid& grid) {

    ClearInterface();
    gridLayout->setContentsMargins(0,50,0,0);
    gridLayout->setHorizontalSpacing(0);
    gridLayout->setVerticalSpacing(0);
    ui->yoDeryabtn->show();
    ui->yoRomainbtn->show();
    for(int  i = 0;i<grid.GetLineNumber();i++)
    {
        for(int j = 0;j<grid.GetColumnNumber();j++)
        {
            std::string ok;
            ok.push_back(grid.GetCell({i,j}));
            QString merde= QString::fromStdString(ok);
            QPushButton* button = new QPushButton(merde);

            QFont font = button->font();
            font.setPointSize(15);
            button->setFont(font);

            if(ok=="x")
            {
                button->setStyleSheet("background-color: rgb(204,255,255)");
            }
            else if(ok=="y")
            {
                button->setStyleSheet("background-color: rgb(255,204,204)");
            }
            else if(ok=="X")
            {
                button->setStyleSheet("background-color: rgb(102,178,255)");
            }
            else if(ok=="Y")
            {
                button->setStyleSheet("background-color: rgb(255,102,102)");
            }

            button->setMinimumSize(135-grid.GetLineNumber()*10,135-grid.GetLineNumber()*10);
            gridLayout->addWidget(button,i,j);
            connect(button , &QPushButton::clicked, [=](){bill2 = true;position = Position(i,j);});
        }
    }
}

void MainWindow::DisplayCurrentPlayer(const Player& player) const {
    QString merde= QString::fromStdString(player.GetName() + " (" + player.GetSymbol() + ") " + "à toi de jouer !");
    ui->yoPaulLbl->setText(merde);
}

void MainWindow::DisplayWinnerMessage(const Player& player) const {
    QString merde= QString::fromStdString("Bravo " + player.GetName() + " , tu as gagné !");
    ui->yoHamzalbl->setText(merde);
}

void MainWindow::DisplayEqualityMessage() const {
    QString merde= QString::fromStdString("Egalité");
    ui->yoHamzalbl->setText(merde);
}

void MainWindow::ClearInterface() {

    ui->yoDeryabtn->hide();
    ui->yoRomainbtn->hide();
    while(gridLayout->count() != 0)
    {
        QLayoutItem* item = gridLayout->itemAt(0);
            gridLayout->removeItem(item);

            // get the widget
            QWidget* widget = item->widget();

            // check if a valid widget
            if(widget)
            {
                delete widget;
            }
    }
    UpdateWindow();
}

// Pr charger un jeu
void MainWindow::OpenFile()
{
    QFileDialog fd(this);
    //fd.setNameFilter();
    QString file;
    if(fd.exec())
    {
        file = fd.selectedFiles()[0];

        gamectrl->SaveGameState(file.toStdString());
    }
}

void MainWindow::UpdateWindow()
{
    this->update();
}

//void MainWindow::QDosDane(bool &bill) const{
//    while(!bill)
//    {

//    }
//}

void MainWindow::on_yoDeryabtn_clicked()
{
    OpenFile();
}




void MainWindow::on_yoRomainbtn_clicked()
{
    gamectrl->RestartGame();
}

