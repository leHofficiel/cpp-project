#ifndef GameMode_h
#define GameMode_h

#include <vector>
#include <iostream>
#include <memory>
#include "../../Models/Player.h"
#include "../../Models/Grid.h"
#include "UserInterface.h"

class GameController;

enum GameState {
    DEFEAT,
    WIN,
    EQUALITY,
    NOTHING
};


class GameMode{
    public :
        virtual inline int GetPlayerNumber() const = 0;
        virtual inline int GetGridColumnNumber() const = 0;
        virtual inline int GetGridLineNumber() const = 0;

        virtual void GridInitState(GameController* ctrl) const = 0;
        virtual void Play(GameController* ctrl) const = 0;
        virtual void AIPlay(GameController* ctrl) const = 0;
        virtual GameState GridState(GameController* ctrl) const = 0;        
};



#endif //GameMode_h