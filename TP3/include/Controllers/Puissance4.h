#ifndef puissance4_h
#define puissance4_h

#include "Interfaces/GameMode.h"
#include <memory>
#include <random>
#include <functional>

class Puissance4 : public GameMode {   
    public :
        Puissance4();                     
        inline int GetGridLineNumber() const { return this->GRID_LINES; }
        inline int GetGridColumnNumber() const { return this->GRID_COLUMNS; }
        inline int GetPlayerNumber() const { return this->PLAYER_NUMBER; };

        void GridInitState(GameController* ctrl) const override;
        void Play( GameController* ctrl) const override;
        void AIPlay( GameController* ctrl) const override;
        GameState GridState(GameController* ctrl) const override;
        

    private :
        const int GRID_LINES = 4;
        const int GRID_COLUMNS = 7;
        const int NB_TOKEN_FOR_WIN = 4;
        const int PLAYER_NUMBER = 2;     

        Position Gravity(Position position, const Grid& grid ) const;
        bool ContainsTokenNumber(std::vector<char> gridPart, char symbol) const;
        
        bool HorizontalCheck(const Grid& grid, char symbol) const;
        bool VerticalCheck(const Grid& grid, char symbol) const;
        bool AscendingDiagonalCheck(const Grid& grid, char symbol) const;
        bool DescendingDiagonalCheck(const Grid& grid, char symbol) const;

        bool Win(const Grid& grid, char symbol) const;
        bool Equality(const Grid& grid) const;
};

#endif //puissance4_h