#ifndef GameController_h
#define GameController_h


#include <cstring>
#include <memory>
#include "../Models/Grid.h"
#include "../Models/Player.h"
#include "../Models/Structs.h"
#include "../Utils/Sauvegarder.h"

#include "Interfaces/GameMode.h"

#include "Morpion.h"
#include "Puissance4.h"
#include "Ladies.h"
#include "Othello.h"

#include <qt6/QtCore/QFile>
#include <qt6/QtCore/QJsonObject>
#include <qt6/QtCore/QJsonDocument>
#include <qt6/QtCore/QJsonArray>
#include <qt6/QtCore/QJsonValue>
#include <qt6/QtCore/QString>
#include <iostream>


class GameController {

public:

    GameController();

    inline std::shared_ptr<UserInterface> GetUserInterface() const{
        return this->userInterface;
    }

    inline std::vector<Player> GetPlayers() const {
        return this->players;
    }

    inline Position GetLastPosition() const {
        return this->lastPosition;
    }

    inline Player GetCurrentPlayer() const {
        return this->players[currentPlayerIndex];            
    }

    inline Grid& GetGrid() {
        return grid;
    }
    
    inline void SetUserInterface(std::shared_ptr<UserInterface> userInterface){
        this->userInterface = userInterface;
        this->userInterface->RegisterController(this);
    }

    inline bool IsFirst(const Player& player) const {
        return player.GetSymbol() == syms[0];
    }
    
    Player GetOtherPlayer(const Player& player) const;  
    void Play();        

    void RestartGame();
    void SaveGameState(std::string path);


private:
    std::vector<char> syms = {
            'x',
            'o'
    };

    const std::vector<std::string> START_GAME_ACTION_LIST {
            "Nouvelle partie",
            "Charger une sauvegarde"
    };

    const std::vector<std::string> END_GAME_ACTION_LIST {
            "Rejouer la même partie",
            "Rejouer un nouvelle partie avec de nouveaux joueurs",
            "Rejouer à un autre jeu",
            "Quitter"
    };
    const std::vector<std::string> GAME_CHOICE_LIST {
            "Morpion",
            "Puissance 4",
            "Othello",
            "Ladies"
    };
    const std::vector<std::string> PLAYER_CREATION_ACTION_LIST {
            "Ajouter un joueur",
            "Ajouter un Bot"
    };

    std::shared_ptr<GameMode> gameType;
    std::vector<Player> players;
    std::shared_ptr<UserInterface> userInterface;
    Grid grid;

    bool isInit;        
    Position lastPosition;
    int currentPlayerIndex;
    int gameId;

    
    void FindWinner();
    bool FindGameState(const Player& player);              
    void PlayRound();        
    QJsonObject toJson();

    // START
    void Init();

    // FOR NEW GAME
    void InitNewGame();
    void SelectGameMode();
    void CreateNewPlayers();

    // FOR LOAD GAME
    void LoadGame();
    void LoadPlayers(QJsonArray players);
    void LoadGrid(QJsonArray grid);

    // COMMON FUNCTIONS
    void InitGameMode(int gameId);
    void CreatePlayer(std::string name, char symbol, bool isBot);
                
        
};

#endif //GameController_h